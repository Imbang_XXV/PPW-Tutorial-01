from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,friend_list,friend_list_json,add_friend,delete_friend,validate_npm,pagePaginator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend

# Create your tests here.
class Lab7UnitTest(TestCase):
	
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_lab7_using_right_template(self):
		response = Client().get('/lab-7/')
		response = self.client.get('/lab-7/')
		self.assertTemplateUsed(response, 'lab_7/lab_7.html')

	def test_paginator(self):
		data = ['a','b','c','d','e','f']
		page1 = pagePaginator(data,1)
		pageDefault = pagePaginator(data,"halo")
		self.assertEqual(page1.object_list, list(data))
		self.assertEqual(pageDefault.object_list, list(data))
		
	def test_can_add_friend(self):
		response = Client().get('/lab-7/')
		data = {'friend_name': 'Imbang','npm': '1606889502'};
		response_post = self.client.post('/lab-7/add-friend/', data)
		self.assertEqual(data,Friend.objects.get(npm='1606889502').as_dict())

	def test_cant_add_friend_with_identical_npm(self):
		response = Client().get('/lab-7/')
		data1 = {'friend_name': 'Imbang','npm': '1606889502'};
		data2 = {'friend_name': 'Imbang','npm': '1606889502'};
		response_post = self.client.post('/lab-7/add-friend/', data1)
		response_post = self.client.post('/lab-7/add-friend/', data2)
		self.assertEqual(1,len(Friend.objects.filter(npm='1606889502')))
			
	def test_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_get_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="Pikachu", npm="12211221")
		response = Client().post('/lab-7/friend-list/delete-friend/' + str(friend.npm) + '/')
		self.assertEqual(response.status_code, 302)
		self.assertNotIn(friend, Friend.objects.all())

	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})
