from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
from api_csui_helper.csui_helper import CSUIhelper
import os

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    friend_list = Friend.objects.all()
    page = request.GET.get('page')

    response['mahasiswa_list'] = pagePaginator(mahasiswa_list,page)
    response['friend_list'] = friend_list
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def pagePaginator(data_list,page):
    paginator = Paginator(data_list,15)
    try:
        new_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        new_list = paginator.page(1)
    return new_list

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        name = request.POST['friend_name']
        exist = Friend.objects.filter(npm=npm).exists()
        if(exist!=True):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            return JsonResponse(friend.as_dict())
        else: return JsonResponse(None,safe = False)

def delete_friend(request, friend_id):
    Friend.objects.get(npm=friend_id).delete()
    return HttpResponseRedirect('/lab-7/friend-list')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)
