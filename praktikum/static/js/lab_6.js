$(document).ready(function() {
	$('.my-select').select2();

	// Feature detect + local reference
	var storage;
	var fail;
	var uid;
	try {
		uid = new Date;
		(storage = window.localStorage).setItem(uid, uid);
		fail = storage.getItem(uid) != uid;
		storage.removeItem(uid);
		fail && (storage = false);
	} catch (exception) {}
	
	//Theme selector
	var themeSelection =[
					    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
					    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
					    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
					    {"id":3,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
					    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
					    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
					    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
					    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
					    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
					    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
					    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
						];

	storage.setItem("themeSelection", JSON.stringify(themeSelection));
		

	var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
	storage.setItem("theme", JSON.stringify(selectedTheme));

	//Set theme selection
	$('.my-select').select2({
		'data': JSON.parse(storage.getItem('themeSelection'))
	});

	var currentTheme = selectedTheme;
	var bgColor;
	for(color in currentTheme){
		bgColor = currentTheme[color].bcgColor;
	}

	$('body').css({'background-color':bgColor});

	$('.apply-button').on('click', function(){
		var newTheme = $('.my-select').val();
		var k;
		var background = {};
	
		// Cari objek tersebut di dalam kamus JSON tema
		var bcgColor = themeSelection[newTheme].bcgColor;
		var fontColor = themeSelection[newTheme].fontColor;
		var text = themeSelection[newTheme].text;
		$("body").css({"background-color": bcgColor});
		$("footer").css({"color":fontColor});
		background[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
		storage.setItem('theme', JSON.stringify(background));
	});


    // kode jQuery selanjutnya akan ditulis disini
	//Chat Message
    counter = 0;
     $('textarea').on('keypress', function (e) {
         if(e.which === 13){
            //Disable textbox to prevent multiple submit
            $(this).attr("disabled", "disabled");

            //Do Stuff, submit, etc..
            if((counter%2)==1){
          		jQuery("<div/>",{
                	class: "msg-send",
                	text: $("textarea")[0].value
            	}).appendTo(".msg-insert");  	
            }else{
            	jQuery("<div/>",{
                	class: "msg-receive",
                	text: $("textarea")[0].value
            	}).appendTo(".msg-insert");  
            }

            counter++;
            //Enable the textbox again if needed.
            $("textarea").val('');
            $(this).removeAttr("disabled");
         }
   });

   $('.my-select').select2();
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
	if (x === 'ac') {
		/* implementasi clear all */
		print.value = '';
	} else if (x === 'eval') {
		if (print.value === ''){
			print.value = '';
		} else if(print.value.includes('sin') || print.value.includes('tan') || print.value.includes('log')){
			var valueStart = print.value.indexOf('(')+1;
			var valueEnd = print.value.indexOf(')');
			if (print.value.includes('sin')) {
                print.value = Math.sin(print.value.substring(valueStart, valueEnd));
            } else if (print.value.includes('tan')) {
                print.value = Math.tan(print.value.substring(valueStart, valueEnd));
            } else if (print.value.includes('log')) {
                print.value = Math.log10(print.value.substring(valueStart, valueEnd));
            }
		} else{
			print.value = Math.round(evil(print.value) * 10000) / 10000;
		}
		erase = true;
	} else{
		if (erase){
			print.value = '';
			erase = false;
		}
		print.value += x;
	}
};

function evil(fn) {
	return new Function('return ' + fn)();
}
// END
