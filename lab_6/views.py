from django.shortcuts import render
from lab_1.views import mhs_name
# Create your views here.
def index(request):
    response = {'author': mhs_name}
    return render(request, 'lab_6.html', response)